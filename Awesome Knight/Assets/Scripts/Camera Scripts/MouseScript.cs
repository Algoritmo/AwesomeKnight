﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseScript : MonoBehaviour {
    public Texture2D cursorTexture;
    private CursorMode mode = CursorMode.ForceSoftware;
    private Vector2 hotSpot = Vector2.zero;

    public GameObject mousePoint;
    private GameObject instantiated;


	// Use this for initialization
	void Start () {
        GameObject cursorObject = GameObject.FindGameObjectWithTag("Cursor");
        Destroy(cursorObject, 2.5f);

    }
	
	// Update is called once per frame
	void Update () {
        //Cursor.SetCursor(cursorTexture, hotSpot, mode);
        if ( Input.GetMouseButtonUp(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if ( Physics.Raycast( ray, out hit)) {
                if ( hit.collider is TerrainCollider) {
                    Vector3 temp = hit.point;
                    temp.y = .1f;

                    if ( instantiated != null ) {
                        instantiated = Instantiate(mousePoint, temp, Quaternion.identity) as GameObject;
                    } else {
                        Destroy(instantiated);
                        instantiated = Instantiate(mousePoint, temp, Quaternion.identity) as GameObject;
                    }
                }
            }
        }
    }
}
