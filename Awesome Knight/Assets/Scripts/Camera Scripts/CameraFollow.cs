﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public float followHeight = 10f;
    public float followDistance = 10f;

    private float targetHeight;
    private float currentHeight;
    private float currentRotation;


    private Transform player;

    private Vector3 targetPosition;

    private Quaternion euler;

	void Awake () {
        player = GameObject.FindGameObjectWithTag("Player").transform ;
	}

    private void OnEnable() {
        
    }

    private void Start() {
        
    }

    // Update is called once per frame
    void Update () {
        targetHeight = player.position.y + followHeight;
        currentRotation = transform.eulerAngles.y;
        currentHeight = Mathf.Lerp(
            transform.position.y,
            targetHeight,
            .9f * Time.deltaTime);

        euler = Quaternion.Euler(0f, currentRotation, 0f);
        targetPosition = player.position - (euler * Vector3.forward) * followDistance;

        targetPosition.y = currentHeight;
        transform.position = targetPosition;
        transform.LookAt(player);
    }
}
