﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {
    public float health = 100f;

    public Image lifeBarImg;

    public void TakeDamage ( float amount) {
        health -= amount;

        lifeBarImg.fillAmount = health / 100f;

        print("Enemy took damage, health is  " + health);

        if ( health <= 0) {

        }
    }
}
