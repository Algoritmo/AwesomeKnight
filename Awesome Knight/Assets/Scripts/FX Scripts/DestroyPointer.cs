﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPointer : MonoBehaviour {
    private Transform player;


	// Use this for initialization
	void Awake () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if ( Vector3.Distance( transform.position, player.position ) <= 2f) {
            Destroy(gameObject);
        }
	}
}
