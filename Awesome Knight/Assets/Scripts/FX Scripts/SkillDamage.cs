﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDamage : MonoBehaviour {
    public LayerMask enemyLayer;
    public float radius = .5f;
    public float damageCount = 10f;

    private EnemyHealth enemyHealth;
    private bool collided;

    private Collider[] hits;



    void Update () {
        hits = Physics.OverlapSphere(transform.position, radius, enemyLayer);

        foreach( Collider collider in hits ) {
            //        if  ( collider.isTrigger) {
            //          continue;
            //    }

            enemyHealth = collider.GetComponent<EnemyHealth>();
            collided = true;

            if ( collided) {
                enemyHealth.TakeDamage(damageCount);
                enabled = false;
            }
        }
	}
	
	
}
