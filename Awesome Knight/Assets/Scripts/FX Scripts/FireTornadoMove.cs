﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTornadoMove : MonoBehaviour {
    public LayerMask enemyLayer;

    public float radius = .5f;
    public float damageCount = 10f;
    public GameObject fireExplotion;

    private EnemyHealth enemyHealth;
    private bool collided;

    private float speedVariable = 3f;


	// Use this for initialization
	void Start () {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        transform.rotation = Quaternion.LookRotation(player.transform.forward);
	}
	
	// Update is called once per frame
	void Update () {
        Move();
        CheckForDamage();
	}


    void CheckForDamage() {
        Collider[] hits = Physics.OverlapSphere(transform.position, radius, enemyLayer);

        foreach(Collider collider in hits) {
            enemyHealth = collider.gameObject.GetComponent<EnemyHealth>();
            collided = true;
        }

        if (collided ) {
            enemyHealth.TakeDamage(damageCount);
            Vector3 temp = transform.position;
            temp.y += 2f;
            Instantiate(fireExplotion, temp, Quaternion.identity);
            Destroy(gameObject);
        }

    }

    void Move() {
        transform.Translate(Vector3.forward * (speedVariable * Time.deltaTime));
    }
}
