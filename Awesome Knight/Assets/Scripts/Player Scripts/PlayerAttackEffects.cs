﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackEffects : MonoBehaviour {
    public GameObject groundImpactSpawn;
    public GameObject kickFXSpawn;
    public GameObject fireTornadoSpawn;
    public GameObject fireShieldSpawn;

    public GameObject groundImpactPrefab;
    public GameObject kickFXPrefab;
    public GameObject fireShieldPrefab;
    public GameObject fireTornadoPrefab;
    public GameObject healFXPrefab;
    public GameObject thunderFXPrefab;


    // Use this for initialization
    void Start () {
		
	}
	
    void GroundImpact () {
        Instantiate( groundImpactPrefab, groundImpactSpawn.transform.position, Quaternion.identity);
    }

    void Kick () {
        Instantiate( kickFXPrefab, kickFXSpawn.transform.position, Quaternion.identity);
    }

    void FireTornado () {
        Instantiate( fireTornadoPrefab, fireTornadoSpawn.transform.position, Quaternion.identity);
    }

    void FireShield() {
        GameObject fireObject = Instantiate(
            fireShieldPrefab, fireShieldSpawn.transform.position, Quaternion.identity) as GameObject;

        fireObject.transform.SetParent(transform);
    }

    void Heal() {
        Vector3 temp = transform.position;
        temp.y += 2f;
        GameObject healObject = Instantiate(healFXPrefab, temp, Quaternion.identity) as GameObject;

        healObject.transform.SetParent(transform);
    }


    void ThunderAttack() {
        Vector3 pos;

        for ( int i = 0; i <8; i++) {
            pos = Vector3.zero;

            if ( i==0 ) {
                pos = new Vector3(
                    transform.position.x - 4f,
                    transform.position.y + 2f,
                    transform.position.z);

            } else if (i == 1) {
                pos = new Vector3(
                    transform.position.x + 4f,
                    transform.position.y + 2f,
                    transform.position.z);

            } else if (i == 2) {
                pos = new Vector3(
                    transform.position.x,
                    transform.position.y + 2f,
                    transform.position.z -4f);

            } else if (i == 3) {
                pos = new Vector3(
                    transform.position.x,
                    transform.position.y + 2f,
                    transform.position.z + 4f);

            } else if (i == 4) {
                pos = new Vector3(
                    transform.position.x + 2.5f,
                    transform.position.y + 2f,
                    transform.position.z + 2.5f);

            } else if (i == 5) {
                pos = new Vector3(
                    transform.position.x -2.5f,
                    transform.position.y + 2f,
                    transform.position.z +2.5f);

            } else if (i == 6) {
                pos = new Vector3(
                    transform.position.x - 2.5f,
                    transform.position.y + 2f,
                    transform.position.z - 2.5f);

            } else if (i == 7) {
                pos = new Vector3(
                    transform.position.x + 2.5f,
                    transform.position.y + 2f,
                    transform.position.z + 2.5f);
            }

            Instantiate(thunderFXPrefab, pos, Quaternion.identity);
        }
    }
}
