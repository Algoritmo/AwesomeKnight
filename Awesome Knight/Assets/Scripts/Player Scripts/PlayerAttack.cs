﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAttack : MonoBehaviour {
    public Image fillWait01;
    public Image fillWait02;
    public Image fillWait03;
    public Image fillWait04;
    public Image fillWait05;
    public Image fillWait06;

    private int[] fadeImages = new int[] { 0, 0, 0, 0, 0, 0 };
    private Animator anim;
    private bool canAttack = true;
    private PlayerMove playerMove;


    private void Awake() {
        anim = GetComponent<Animator>();
        playerMove = GetComponent<PlayerMove>();
    }
    
    void Start () {
		
	}
	
	void Update () {
		if(!anim.IsInTransition(0) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand")) {
            canAttack = true;
        } else {
            canAttack = false;
        }

        CheckToFade();
        CheckInput();
	}

    void CheckInput() {
        if (anim.GetInteger("Atk") == 0) {
            playerMove.FinishedMovement = false;

            if (!anim.IsInTransition(0) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand")) {
                playerMove.FinishedMovement = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1)) {
                playerMove.TargetPosition = transform.position;

        if (playerMove.FinishedMovement
                    && fadeImages[0] != 1
                    && canAttack) {
                    fadeImages[0] = 1;
                    anim.SetInteger("Atk", 1);
        }

        } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
                playerMove.TargetPosition = transform.position;

                if (playerMove.FinishedMovement
                    && fadeImages[1] != 1
                    && canAttack) {
                    fadeImages[1] = 1;
                    anim.SetInteger("Atk", 2);
                }

        } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
                playerMove.TargetPosition = transform.position;

                if (playerMove.FinishedMovement
                    && fadeImages[2] != 1
                    && canAttack) {
                    fadeImages[2] = 1;
                    anim.SetInteger("Atk", 3);
                }

        } else if (Input.GetKeyDown(KeyCode.Alpha4)) {
                playerMove.TargetPosition = transform.position;

                if (playerMove.FinishedMovement
                    && fadeImages[3] != 1
                    && canAttack) {
                    fadeImages[3] = 1;
                    anim.SetInteger("Atk", 4);
                }

        } else if (Input.GetKeyDown(KeyCode.Alpha5)) {
                playerMove.TargetPosition = transform.position;

                if (playerMove.FinishedMovement
                    && fadeImages[4] != 1
                    && canAttack) {
                    fadeImages[4] = 1;
                    anim.SetInteger("Atk", 5);
                }

        } else if (Input.GetKeyDown(KeyCode.Alpha6)) {
                playerMove.TargetPosition = transform.position;

                if (playerMove.FinishedMovement
                    && fadeImages[5] != 1
                    && canAttack) {
                    fadeImages[5] = 1;
                    anim.SetInteger("Atk", 6);
                }

        } else {
                anim.SetInteger("Atk", 0);
        }

        if (Input.GetKey(KeyCode.Space)) {
                Vector3 targetPos = Vector3.zero;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit)) {
                    targetPos = new Vector3(hit.point.x,
                        transform.position.y,
                        hit.point.z);
                }

                transform.rotation = Quaternion.Slerp(transform.rotation,
                    Quaternion.LookRotation(targetPos - transform.position),
                    15f * Time.deltaTime);
        }
        
    }


    void CheckToFade () {
        if (fadeImages[0] == 1) {
            if (FadeAndWait(fillWait01, 1f)) {
                fadeImages[0] = 0;
            }
        }

        if (fadeImages[1] == 1) {
            if (FadeAndWait(fillWait02, 1f)) {
                fadeImages[1] = 0;
            }
        }

        if (fadeImages[2] == 1) {
            if (FadeAndWait(fillWait03, 1f)) {
                fadeImages[2] = 0;
            }
        }

        if (fadeImages[3] == 1) {
            if (FadeAndWait(fillWait04, 1f)) {
                fadeImages[3] = 0;
            }
        }

        if (fadeImages[4] == 1) {
            if (FadeAndWait(fillWait05, 1f)) {
                fadeImages[4] = 0;
            }
        }
        if (fadeImages[5] == 1) {
            if (FadeAndWait(fillWait06, 1f)) {
                fadeImages[5] = 0;
            }
        }
    }


    bool FadeAndWait( Image fadeImage, float fadeTime ) {
        bool faded = false;

        if (fadeImage == null)
            return faded;

        if (!fadeImage.gameObject.activeInHierarchy ) {
            fadeImage.gameObject.SetActive(true);
            fadeImage.fillAmount = 1f;
            faded = false;
        }

        fadeImage.fillAmount -= fadeTime * Time.deltaTime;

        if (fadeImage.fillAmount <= 0f ) {
            fadeImage.gameObject.SetActive(false);
            faded = true;
        }

        return faded;
    }


}
