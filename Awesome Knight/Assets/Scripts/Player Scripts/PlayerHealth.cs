﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    public float health = 100f;

    private bool isShielded;

    private Animator anim;

    private Image playerHealthImg;

    private void Awake() {
        anim = GetComponent<Animator>();
        playerHealthImg = GameObject.Find("HealthIcon").GetComponent<Image>();
    }

    public bool Shielded {
        get { return isShielded; }
        set { isShielded = value; }
    }

    public void TakeDamage(float amount) {
        if (!isShielded) {
            health -= amount;
            playerHealthImg.fillAmount = health / 100f;

            print("player health = " + health);

            if (health <= 0f) {
                anim.SetBool("Death", true);

                if (!anim.IsInTransition(0) && anim.GetCurrentAnimatorStateInfo(0).IsName("Death")
                         && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= .95f ) {
                    Destroy(gameObject, 2f);
                }
            }
        }
    }

    
    public void Heal(float amount) {
        health += amount;

        if (health > 100f)
            health = 100f;

        playerHealthImg.fillAmount = health / 100;
    }
}
